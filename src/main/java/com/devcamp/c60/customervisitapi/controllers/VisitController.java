package com.devcamp.c60.customervisitapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c60.customervisitapi.models.Visit;
import com.devcamp.c60.customervisitapi.services.VisitService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class VisitController {
    @Autowired
    VisitService visitService;

    @GetMapping("/visits")
    public ArrayList<Visit> getAllVisit(){
        ArrayList<Visit> allVisit = visitService.getAllVisit();
        
        return allVisit;
    }
}
