package com.devcamp.c60.customervisitapi.services;

import java.sql.Date;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.c60.customervisitapi.models.Visit;

@Service
public class VisitService {
    @Autowired
    CustomerService customerService;

    public ArrayList<Visit> getAllVisit() {
        ArrayList<Visit> allVisit = new ArrayList<Visit>();

        

        
        String dateString = "2023-06-07";
        Date date2 = Date.valueOf(dateString);

        allVisit.add(new Visit(customerService.customer1, null));
        allVisit.add(new Visit(customerService.customer2, date2));
        allVisit.add(new Visit(customerService.customer3, date2));

        return allVisit;
    }
}
